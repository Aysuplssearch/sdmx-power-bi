# SDMX Power BI connector
This is the SDMX Power BI connector. The aim is to make it really simple for Power-BI report builders to quickly get data from an SDMX web service without having to bother about technical things like HTTP headers and parsing labels from codes. It can connect to any SDMX REST web service that supports the standard SDMX REST HTTP headers and can output SDMX-CSV (such as the NSI web service that is implemented by .Stat and many other platforms). 

Go to the [documentation](https://sis-cc.gitlab.io/sdmx-tools/documentation/using-sdmx-powerbi-connector/) for [installation](https://sis-cc.gitlab.io/sdmx-tools/documentation/using-sdmx-powerbi-connector/installation-instructions/) and [usage](https://sis-cc.gitlab.io/sdmx-tools/documentation/using-sdmx-powerbi-connector/how-to-use/) instructions.

