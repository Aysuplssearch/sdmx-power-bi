﻿[Version = "1.0.0"]
section SDMX;

[DataSource.Kind="SDMX", Publish="SDMX.Publish"]
shared SDMX.Contents =  Value.ReplaceType( GetData, GetDataType);

DisplayFormatList =  { 
{"Show codes and labels", "application/vnd.sdmx.data+csv;file=true;labels=both"},
{"Show codes only", "application/vnd.sdmx.data+csv;labels=id"},
{"Show labels only","application/vnd.sdmx.data+csv;file=true;labels=both"}
};

DisplayFormatLabels= List.Transform( DisplayFormatList, each _{0});

GetDataType = type function (
    url as (type text meta [
    Documentation.FieldCaption = Extension.LoadString("FunctionParameterURL"),
    Documentation.SampleValues = {"https://<SDMX RESTful data query>"},
    DataSource.Path=false] ),
    Option as (type text meta [
    Documentation.FieldCaption = Extension.LoadString("FunctionParameterOption"),
    DataSource.Path=false,
    Documentation.AllowedValues = DisplayFormatLabels]),
    optional Language as (type text meta [
    Documentation.FieldCaption = Extension.LoadString("FunctionParameterLanguage"),
    Documentation.SampleValues = {"fr-CH, fr, en"},
    DataSource.Path=false]))
    as table meta [
    Documentation.Name = Extension.LoadString("FunctionName"),
        Documentation.LongDescription = Extension.LoadString("FunctionLongDescription"),
        Documentation.Examples = {[
            Description = Extension.LoadString("FunctionExampleLongDescription"),
            Code = "SDMX.Contents(url,""Show codes and labels"",""en"")",
            Result = Extension.LoadString("FunctionExampleResult")
        ]}
    ];


GetData= (url as text, Option as text, optional Language as text) => let
    Source = List.Select(DisplayFormatList, each _{0} = Option){0}{1},
    myHeaders = if Language = null then [Accept=Source] else [Accept=Source] & [#"Accept-Language"= Language],
    _url = ValidateUrlScheme(url),
    Custom1 = Web.Contents(_url, [Headers=myHeaders]),
    myDelimiter = Text.At( Lines.FromBinary( Custom1){0},8),
    Custom2 = Csv.Document( Custom1, null, myDelimiter ),
    x = Table.PromoteHeaders(Custom2, [PromoteAllScalars=true]),
    
    Custom3 = if Option = "Show labels only" then TransformToLabelsOnly( x ) else x
in
    Custom3;



TransformToLabelsOnly = (x as table) as table =>
let
    OriginalHeaders = List.RemoveItems(  Table.ColumnNames( x ),{"DATAFLOW","OBS_VALUE"}),
    NewHeaderNames = List.Transform( OriginalHeaders, each if try Number.From(Text.AfterDelimiter(_, ":")) = null otherwise false  then _ else Text.TrimStart( Text.AfterDelimiter(_, ":" ))), 
    RenamedHeaders = Table.RenameColumns( x, List.Zip({OriginalHeaders,NewHeaderNames})),
    IterationForColumns = List.Accumulate( NewHeaderNames,  
  RenamedHeaders ,
(state, column) => Table.TransformColumns(state, {column, each if Text.Contains(_,":") then Text.TrimStart(Text.AfterDelimiter(_, ":")  ) else _} )
)
in
    IterationForColumns;

ValidateUrlScheme = (url as text) as text => if (Uri.Parts(url)[Scheme] <> "https") then error "Url scheme must be HTTPS" else url;

// Data Source Kind description
SDMX = [
TestConnection = (dataSourcePath) as list => { "SDMX.Contents", "https://www.ilo.org/sdmx/rest/data/ILO,DF_YI_ALL_LAC_4HRL_ECO_CUR_NB/?format=csv&startPeriod=1971-01-01&endPeriod=2021-12-31","Show codes and labels" , "null"},
    Authentication = [
        Implicit = []
    ],
    Label = Extension.LoadString("DataSourceLabel")
];

// Data Source UI publishing description
SDMX.Publish = [
    Beta = true,
    Category = "Other",
    ButtonText = { Extension.LoadString("ButtonTitle"), Extension.LoadString("ButtonHelp") },
    LearnMoreUrl = " https://sdmx.org/",
    SourceImage = SDMX.Icons,
    SourceTypeImage = SDMX.Icons
];

SDMX.Icons = [
    Icon16 = { Extension.Contents("SDMX16.png"), Extension.Contents("SDMX20.png"), Extension.Contents("SDMX24.png"), Extension.Contents("SDMX32.png") },
    Icon32 = { Extension.Contents("SDMX32.png"), Extension.Contents("SDMX40.png"), Extension.Contents("SDMX48.png"), Extension.Contents("SDMX64.png") }
];
